<?php

declare(strict_types=1);

namespace Bittacora\LaravelTestingHelpers\Views;

use Illuminate\View\Factory;
use Illuminate\View\FileViewFinder;

class VendorViewsDisabler
{
    /**
     * Deshabilita las vistas de resources/views/vendor/*.
     */
    public static function disableViews(): void
    {
        /** @var Factory $view */
        $view = resolve(Factory::class);
        /** @var FileViewFinder $finder */
        $finder = $view->getFinder();
        $currentHints = $finder->getHints();
        foreach ($currentHints as $namespace => $hints) {
            $hints = array_filter($hints, fn($item) => !str_contains($item, 'resources/views/vendor'));
            $finder->replaceNamespace($namespace, $hints);
        }
    }
}