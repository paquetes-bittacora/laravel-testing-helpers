<?php

declare(strict_types=1);

namespace Bittacora\LaravelTestingHelpers\Permissions;

use Illuminate\Contracts\Auth\Authenticatable;
use Spatie\Permission\Models\Permission;

final class PermissionTestHelper
{
    /**
     * Da permiso a un Authenticatable. Si el permiso no existe, lo crea.
     */
    public static function givePermissionTo(Authenticatable $user, string $permission): void
    {
        $user->givePermissionTo(Permission::firstOrCreate(['name' => $permission]));
    }

}