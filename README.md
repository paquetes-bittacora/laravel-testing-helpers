# Laravel Testing Helpers

Paquete con funciones auxiliares para facilitar el testing de los paquetes de bPanel 4.

## Deshabilitar vistas del proyecto

Al registrar vistas desde un paquete estas pueden anularse y usar otras específicas
del proyecto copiándolas en `resources/views/vendor` [como se indica en la documentación](https://laravel.com/docs/10.x/packages#views).

Aunque esto es muy útil para personalizar las vistas de un paquete, puede hacer que 
algunos tests fallen, si por ejemplo en las vistas modificadas no se incluye algún texto
al que se hace referencia en un test, etc. 

Para ignorar las vistas de `resources/views/vendor`, se puede llamar a:

```
\Bittacora\LaravelTestingHelpers\Views\VendorViewsDisabler::disableViews()
```

Una vez hecha esa llamada, ya solo se usarán las vistas propias de cada paquete.